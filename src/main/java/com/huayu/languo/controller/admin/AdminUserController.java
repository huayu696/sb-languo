package com.huayu.languo.controller.admin;

import com.huayu.languo.controller.base.AbstractBaseAdminController;
import com.huayu.languo.model.dto.ResultDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 刘雄康
 * @version v1.0
 * @description 应用人员控制类
 * @date 2019年05月07日
 */
//@ReqMappingRestController("uu")
@RestController("/appuser")
@RequestMapping("uu")
public class AdminUserController extends AbstractBaseAdminController {


    @GetMapping("/testadmin")
    public ResultDto delete2() {
        logger.info("---- testadmin  22222----");
        return ResultDto.success();
    }
}
