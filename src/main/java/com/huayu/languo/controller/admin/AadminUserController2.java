package com.huayu.languo.controller.admin;

import com.huayu.languo.controller.base.AbstractBaseAdminController;
import com.huayu.languo.model.anno.ReqMappingRestController;
import com.huayu.languo.model.dto.ResultDto;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author 刘雄康
 * @version v1.0
 * @description 应用人员控制类
 * @date 2019年05月07日
 */
@ReqMappingRestController("/uu2")
//@RestController("appuser22")
//@RequestMapping("uu2")
public class AadminUserController2 extends AbstractBaseAdminController {

    @GetMapping("/testadmin")
    public ResultDto delete2() {
        logger.info("---- testadmin  22222----");
        return ResultDto.success();
    }
}
