package com.huayu.languo.controller.api;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.huayu.languo.controller.base.AbstractBaseApiController;
import com.huayu.languo.handler.service.SpUserService;
import com.huayu.languo.model.dto.ResultDto;
import com.huayu.languo.model.po.SpUser;
import org.hibernate.validator.constraints.Range;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;


/**
* @description sp_user（sp用户注释） 访问控制类
* @date     2019-05-09
* @author   liuxiongkang
* @version  v1.0
*/
//@ReqMappingRestController("user1")
@RestController
@RequestMapping("user1")
@Validated
public class SpUserController extends AbstractBaseApiController {

    @Resource
    private SpUserService spUserService;

    @GetMapping("/{id}")
    public ResultDto detail(@PathVariable Integer id) {
        SpUser spUser = spUserService.selectTest(1L);
        SpUser byId = spUserService.getById(id);
        return ResultDto.success(byId);
    }

    /**如果只有少数对象，直接把参数写到Controller层，然后在Controller层进行验证就可以了。*/
    @GetMapping(value = "/demo3")
    public void demo3(@Range(min = 1, max = 9, message = "年级只能从1-9") @RequestParam(name = "grade", required = true) int grade,
                    @Min(value = 1, message = "班级最小只能1") @Max(value = 99, message = "班级最大只能99") @RequestParam(name = "classroom", required = true) int classroom) {
        System.out.println(grade + "," + classroom);
    }

    @GetMapping("/testapi")
    public ResultDto delete2() {
        logger.info("----  testapi  11111111----");
        return ResultDto.success();
    }

    @PostMapping("/add")
    public ResultDto add(@RequestBody SpUser spUser) {
        spUserService.save(spUser);
        return ResultDto.success();
    }

    @DeleteMapping("/{id}")
    public ResultDto delete(@PathVariable Integer id) {
        spUserService.removeById(id);
        return ResultDto.success();
    }

    @PutMapping
    public ResultDto update(@RequestBody SpUser spUser) {
        spUserService.update(new UpdateWrapper(spUser));
        return ResultDto.success();
    }


    @GetMapping
    public ResultDto list(@RequestParam(defaultValue = "0") Integer page, @RequestParam(defaultValue = "0") Integer size) {
        //PageHelper.startPage(page, size);
        //List<SpUser> list = spUserService.findAll();
        //PageInfo pageInfo = new PageInfo(list);
        return ResultDto.success();
    }

}
