package com.huayu.languo.controller.api;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.huayu.languo.controller.base.AbstractBaseApiController;
import com.huayu.languo.handler.service.SpUserService;
import com.huayu.languo.mapper.ProductConfigMapper;
import com.huayu.languo.mapper.SpUserMapper;
import com.huayu.languo.model.anno.ReqMappingRestController;
import com.huayu.languo.model.dto.ResultDto;
import com.huayu.languo.model.enums.SexEnum;
import com.huayu.languo.model.po.SpUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.ArrayList;


/**
 * @author liuxiongkang
 * @version v1.0
 * @description sp_user（sp用户注释） 访问控制类
 * @date 2019-05-09
 */
@ReqMappingRestController({"user2", "user2tttest"})
@Validated
public class SpUserCacheController extends AbstractBaseApiController {

    @Resource
    private SpUserService spUserService;

    //测试缓存
    @Autowired
    private SpUserMapper spUserMapper;


    @Resource
    private ProductConfigMapper productConfigMapper;

    //清除缓存
    //@Autowired
    private CacheManager cacheManager;

    @ResponseBody
    @RequestMapping("/cache")
    public String cache(int id) {
        return spUserMapper.selectCache(id).toString();
    }

    @ResponseBody
    @RequestMapping("/clear")
    public String clear() {
        cacheManager.getCache("ehcache").clear();
        return "success";
    }


    @GetMapping("/{id}")
    public ResultDto detail(@PathVariable Integer id) {
        SpUser byId = spUserService.getById(id);
        SpUser spUser2 = null;
        try {
            spUser2 = spUserService.selectTest(1L);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //插入数据
        SpUser newUser = null;
        try {
            newUser = new SpUser();
            newUser.setUsername("test setUsername");
            newUser.setPassword("test pwd");
            newUser.setNickName("test nick");
            newUser.setSex(SexEnum.NEUTER);
            //newUser.setVersion(0);
            newUser.setDelFlag(1);
            //newUser.setId(0L);
            //newUser.setInTime(LocalDateTime.now());
            //newUser.setUpdTime(LocalDateTime.now());
            spUserService.save(newUser);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Lambda 测试 乐观锁
        try {
            logger.info("newUser id:" + newUser.getId());
            //FIXME 有bug
            //LambdaUpdateWrapper<SpUser> eq = new LambdaUpdateWrapper<SpUser>().set(SpUser::getNickName, "张三111").eq(SpUser::getId, newUser.getId());
            //spUserService.update(eq);
            //
            //LambdaUpdateWrapper<SpUser> eq1 = Wrappers.<SpUser>lambdaUpdate().set(SpUser::getNickName, "张三222").eq(SpUser::getId, newUser.getId());
            //spUserService.update(eq1);
        } catch (Exception e) {
            e.printStackTrace();
        }


        //测试 乐观锁   version 字段必须有值时才触发 乐观锁。 所以需要先查询出 该值
        try {
            logger.info("newUser id:" + newUser.getId());
            newUser.setNickName("乐观锁-逻辑更新");
            boolean b = spUserService.updateById(newUser);
            System.out.println("乐观锁-逻辑更新：" + b);

            UpdateWrapper<SpUser> eq2 = new UpdateWrapper<SpUser>().eq("id", newUser.getId());
            newUser.setNickName("乐观锁-逻辑更新222 +UpdateWrapper");
            boolean remove = spUserService.update(newUser, eq2);
            System.out.println("乐观锁-逻辑更新：" + remove);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //测试逻辑删除
        try {
            logger.info("newUser id:" + newUser.getId());
            UpdateWrapper<SpUser> eq = new UpdateWrapper<SpUser>().set("nick_name", "张三222").eq("id", newUser.getId());
            boolean update = spUserService.update(eq);
            System.out.println("逻辑更新" + update);

            UpdateWrapper<SpUser> eq2 = new UpdateWrapper<SpUser>().eq("id", newUser.getId());
            boolean remove = spUserService.remove(eq2);
            System.out.println("逻辑删除" + remove);
        } catch (Exception e) {
            e.printStackTrace();
        }


        //删除全表试试 ，测试 是 阻断 还是逻辑删除 全部
        try {
            spUserService.remove(null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //删除全表试试 ，测试 是 阻断 还是  删除 全部
        try {
            productConfigMapper.delete(null);
        } catch (Exception e) {
            e.printStackTrace();
        }


        //测试自定义 批量插入
        try {

            SpUser spUser = new SpUser();
            spUser.setUsername("test 2 setUsername");
            spUser.setPassword("test 2 pwd");
            spUser.setNickName("test 2 nick");
            spUser.setSex(SexEnum.NEUTER);
            spUser.setVersion(22);
            spUser.setDelFlag(22);
            ArrayList<SpUser> list = CollUtil.newArrayList(newUser, spUser);
            spUserMapper.insertBatchSomeColumn(list);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResultDto.success(byId);
    }

}
