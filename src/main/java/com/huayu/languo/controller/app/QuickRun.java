package com.huayu.languo.controller.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(value = "/admin/v1")
public class QuickRun {

    private static final Logger LOG = LoggerFactory.getLogger(QuickRun.class);

    @RequestMapping(value = "/first", method = RequestMethod.GET)
    public Map<String, Object> firstResp(HttpServletRequest request) {
        LOG.debug("12312313123 first first");
        LOG.info("12312313123 first first");
        LOG.warn("logback 成功了");
        LOG.error("logback 成功了");

        request.getSession().setAttribute("requestUrl", request.getRequestURL());
        request.getSession().setAttribute("sessionId", request.getSession().getId());

        Map<String, Object> map = new HashMap<>();
        map.put("requestUrl", request.getRequestURL());
        map.put("sessionId", request.getSession().getId());
        return map;
    }

    @RequestMapping(value = "/sessions", method = RequestMethod.GET)
    public Object sessions(HttpServletRequest request) {
        LOG.debug("sessions  sessions  sessions");
        Map<String, Object> map = new HashMap<>();
        map.put("sessionId", request.getSession().getId());
        map.put("sessionId2", request.getSession().getAttribute("sessionId"));
        map.put("requestUrl", request.getSession().getAttribute("requestUrl"));
        return map;
    }
}
