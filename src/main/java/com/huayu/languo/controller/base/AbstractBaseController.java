package com.huayu.languo.controller.base;

import com.huayu.languo.common.framework.RequestContextHolders;
import com.huayu.languo.model.anno.EnhanceReqMapping;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * @Description: 基础控制器, 可以放一些通用方法
 * @Author: lxk
 * @Date: 2019/5/27 10:34
 */
@EnhanceReqMapping("languo")
public abstract class AbstractBaseController {

    protected Logger logger = LoggerFactory.getLogger(this.getClass());

    protected AbstractBaseController() {
        //do noting
    }

    protected HttpServletRequest getHttpRequest() {
        return RequestContextHolders.getRequest();
    }

    protected HttpServletResponse getHttpResponse() {
        return RequestContextHolders.getResponse();
    }

    protected HttpSession getSession() {
        return RequestContextHolders.getSession();
    }
}
