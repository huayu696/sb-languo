package com.huayu.languo.controller.base;

import com.huayu.languo.model.dto.ResultDto;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 *  https://blog.csdn.net/qq_36666651/article/details/81135139
 *  https://blog.csdn.net/L_Sail/article/details/70198886
 * @Description:  404 页面处理
 * @Author: lxk
 * @Date: 2019/5/22 23:18
*/
@RestController
public class BasePageController implements ErrorController {

    @Override
    public String getErrorPath() {
        return "/error";
    }

    @RequestMapping("/error")
    public ResultDto error() {
        return ResultDto.builder().code("404").msg("接口不存在").build();
    }

    @RequestMapping(value = "/error", produces = MediaType.TEXT_HTML_VALUE)
    public ModelAndView errorPage() {
        return new ModelAndView("/404.html");
    }

    @RequestMapping(value = "/", produces = MediaType.TEXT_HTML_VALUE)
    public ModelAndView index() {
        return new ModelAndView("/index.html");
    }

}
