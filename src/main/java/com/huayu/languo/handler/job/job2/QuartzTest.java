/** 
 * @Description:  
 * 
 * @Title: QuartzTest.java 
 * @Package com.joyce.quartz.main 
 * @Copyright: Copyright (c) 2014 
 * 
 * @author Comsys-LZP 
 * @date 2014-6-26 下午03:35:05 
 * @version V2.0 
 */
package com.huayu.languo.handler.job.job2;

/** 
 * @Description: 测试类 
 * 
 * @ClassName: QuartzTest 
 * @Copyright: Copyright (c) 2014 
 *  
 * @version V2.0 
 */
public class QuartzTest {

    public static String JOB_NAME = "动态任务调度";

    public static String TRIGGER_NAME = "动态任务触发器";

    public static String JOB_GROUP_NAME = "XLXXCC_JOB_GROUP";

    public static String TRIGGER_GROUP_NAME = "XLXXCC_JOB_GROUP";

    public static void main(String[] args) {
        try {
            System.out.println("【系统启动】开始(每1秒输出一次)...");
            QuartzManager.addJob(JOB_NAME, JOB_GROUP_NAME, TRIGGER_NAME, TRIGGER_GROUP_NAME, QuartzJob.class, "0/1 * * * * ?");

            Thread.sleep(5000);
            System.out.println("【修改时间】开始(每5秒输出一次)...");
            QuartzManager.modifyJobTime(JOB_NAME, JOB_GROUP_NAME, TRIGGER_NAME, TRIGGER_GROUP_NAME, "0/5 * * * * ?");

            Thread.sleep(6000);
            System.out.println("【移除定时】开始...");
            QuartzManager.removeJob(JOB_NAME, JOB_GROUP_NAME, TRIGGER_NAME, TRIGGER_GROUP_NAME);
            System.out.println("【移除定时】成功");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
