package com.huayu.languo.handler.job.job;

import com.alibaba.fastjson.JSON;
import com.huayu.languo.model.enums.SexEnum;
import com.huayu.languo.model.po.SpUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.stereotype.Component;

import java.util.Date;


@Component
@Slf4j
public class PublishRedisJob {

    @Autowired
    private ChannelTopic userRegisterTopic;

    @Autowired
    private ChannelTopic chatMsgTopic;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    // 向redis消息队列的2个通道发布消息,2分钟一次
    //@Scheduled(fixedRate = 120000)
    public void sendMessage() {
        SpUser spUser = new SpUser();
        spUser.setUsername("setUsername");
        spUser.setPassword("setPassword");
        spUser.setNickName("setNickName");
        spUser.setSex(SexEnum.MAN);
        spUser.setVersion(0);
        spUser.setDelFlag(0);
        spUser.setId(0L);
        spUser.setInTime(new Date());
        spUser.setUpdTime(new Date());
        stringRedisTemplate.convertAndSend(userRegisterTopic.getTopic(), JSON.toJSON(spUser));
        stringRedisTemplate.convertAndSend(chatMsgTopic.getTopic(), String.valueOf(Math.random()));
    }
}
