package com.huayu.languo.handler.job.job;//package com.huayu.languo.biz.job;
//
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.scheduling.Trigger;
//import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
//import org.springframework.scheduling.config.ContextLifecycleScheduledTaskRegistrar;
//import org.springframework.scheduling.config.TriggerTask;
//import org.springframework.scheduling.support.CronTrigger;
//import org.springframework.stereotype.Component;
//
//import java.util.Arrays;
//import java.util.Date;
//
///**
// * @author 刘雄康
// * @version v1.0
// * @description
// * @date 2019年05月09日
// */
//@Slf4j
//@Component
//public class DynamicCronTask {
//
//    private static String cron = "0 35 14 * * ?";
//
//    private static ContextLifecycleScheduledTaskRegistrar taskRegistrar;
//
//    public DynamicCronTask() {
//        configureTasks();
//        new Thread(() -> {
//            try {
//                Thread.sleep(10 * 1000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//            cron = "0 36 14 * * ?";
//            System.err.println("修改cron为：" + cron);
//            configureTasks();
//        }).start();
//    }
//
//    public void configureTasks() {
//        ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
//        scheduler.setPoolSize(3);
//        scheduler.initialize();
//        if (taskRegistrar != null) {
//            taskRegistrar.destroy();
//        }
//        taskRegistrar = new ContextLifecycleScheduledTaskRegistrar();
//        taskRegistrar.setScheduler(scheduler);
//        Runnable runnable = () -> {
//            //任务逻辑
//            log.info("dynamicCronTask is running...");
//        };
//        Trigger trigger = triggercontext -> {
//            log.info("nextExecutionTime:{}", cron);
//            //任务触发，可修改任务的执行周期
//            CronTrigger trigger1 = new CronTrigger(cron);
//            Date nextExec = trigger1.nextExecutionTime(triggercontext);
//            return nextExec;
//        };
//        TriggerTask triggerTask = new TriggerTask(runnable, trigger);
//        taskRegistrar.setTriggerTasksList(Arrays.asList(triggerTask));
//        taskRegistrar.afterSingletonsInstantiated();
//    }
//}
