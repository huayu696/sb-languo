//package com.huayu.languo.handler.batch.batch1;
//
//import org.springframework.batch.core.Job;
//import org.springframework.batch.core.Step;
//import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
//import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
//import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
//import org.springframework.batch.item.ItemReader;
//import org.springframework.batch.item.ItemWriter;
//import org.springframework.batch.item.adapter.ItemWriterAdapter;
//import org.springframework.batch.item.file.FlatFileItemReader;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.ComponentScan;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.core.io.InputStreamResource;
//
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.FileNotFoundException;
//
//@Configuration
//@ComponentScan("com.huayu.languo.handler.batch.batch1")
//@EnableBatchProcessing
//public class SimpleBatchConfiguration {
//
//  @Autowired
//  private JobBuilderFactory job;
//
//  @Autowired
//  private StepBuilderFactory steps;
//
//
//  //配置1个Job,job里只有1个step
//  @Bean
//  public Job simpleJob(Step step) {
//    return job.get("simpleJob").start(step).build();
//  }
//
//  //step 里的reader和writer
//  @Bean
//  protected Step step(ItemReader<String> reader, ItemWriter<Person> writer2) {
//    return steps.get("step1")
//      .<String, Person>chunk(10)
//      .reader(reader)
//      .writer(writer2)
//      .build();
//  }
//
//  //reader 使用sprinb batch内置的FlatFileItemReader，读取1行并封装成为1个Person 对象
//  @Bean
//  protected ItemReader<String> reader() {
//    FlatFileItemReader reader = new FlatFileItemReader();
//    FileInputStream fis = null;
//    try {
//      fis = new FileInputStream(new File("E:\\person.txt"));
//      // 1,Rechard,20
//      // 2,James,30
//      // 3,Cury,28
//      // 4,Durant,26
//    } catch (FileNotFoundException e) {
//      e.printStackTrace();
//    }
//    reader.setResource(new InputStreamResource(fis));
//    reader.setLineMapper((line, number) -> {
//      String[] str = line.split(",");
//      Person p = new Person();
//      p.setId(Integer.parseInt(str[0]));
//      p.setName(str[1]);
//      p.setAge(Integer.parseInt(str[2]));
//      return p;
//    });
//    return reader;
//  }
//
//  //reader 则简单的打印出来，这里是ItemWriterAdapter，这个类主要是设置一个代理类来帮助打印，代理类就是真实的处理Person逻辑的类PersonProcessor
//  @Bean
//  protected ItemWriter<Person> writer2() {
//    ItemWriterAdapter<Person> adapter = new ItemWriterAdapter();
//    adapter.setTargetMethod("print");
//    adapter.setTargetObject(new PersonProcessor());
//    return adapter;
//  }
//
//
//  // @Bean()
//  // @StepScope
//  // public ItemReader<String> itemReader(@Value("#{jobParameters[a]}") String affiliate) {
//  //   String sql = "select * from PENDINGUSER WHERE AFFILIATEID=?";
//  //   return new JdbcCursorItemReaderBuilder<String>().name("dataSourceReader").dataSource(dataSource)
//  //     .sql(sql)
//  //     .preparedStatementSetter(ps -> ps.setString(1, affiliate.toUpperCase()))
//  //     .rowMapper((rs, index) -> {
//  //       return rs.getInt("SSMREQUESTKY") + "";
//  //     }).build();
//  // }
//
//}