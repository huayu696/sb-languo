package com.huayu.languo.handler.batch.batch1;

import lombok.Data;

@Data
public class Person {
  private int id;
  private String name;
  private int age;
  //getter and setter
}