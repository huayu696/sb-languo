//package com.huayu.languo.handler.batch.batch1;
//
//import org.springframework.batch.core.Job;
//import org.springframework.batch.core.JobParameters;
//import org.springframework.batch.core.launch.JobLauncher;
//import org.springframework.context.annotation.AnnotationConfigApplicationContext;
//
//public class SimpleDemoRun1 {
//  public static void main(String[] args) {
//    AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
//    ctx.register(SimpleBatchConfiguration.class);
//    ctx.refresh();
//    JobLauncher launcher = (JobLauncher) ctx.getBean("jobLauncher");
//    JobParameters parameters = new JobParameters();
//    try {
//      launcher.run((Job) ctx.getBean("simpleJob"), parameters);
//    } catch (Exception e) {
//      e.printStackTrace();
//    }
//  }
//}