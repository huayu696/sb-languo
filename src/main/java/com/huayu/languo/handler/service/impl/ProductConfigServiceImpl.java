package com.huayu.languo.handler.service.impl;

import com.huayu.languo.handler.service.ProductConfigService;
import com.huayu.languo.mapper.ProductConfigMapper;
import com.huayu.languo.model.po.ProductConfig;
import org.springframework.stereotype.Service;


@Service("productConfigService")
public class ProductConfigServiceImpl extends AbstractBaseSuperServiceImpl<ProductConfigMapper, ProductConfig>  implements ProductConfigService {

    private ProductConfigMapper productConfigMapper;

    @Override
    public int insert(ProductConfig productConfig) throws Exception {
        return 0;
    }
}
