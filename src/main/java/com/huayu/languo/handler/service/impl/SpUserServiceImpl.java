package com.huayu.languo.handler.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huayu.languo.handler.service.SpUserService;
import com.huayu.languo.mapper.SpUserMapper;
import com.huayu.languo.model.po.SpUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * sp用户注释 服务实现类
 * </p>
 *
 * @author liuxiongkang
 * @since 2019-05-09
 */
@Service
public class SpUserServiceImpl extends AbstractBaseSuperServiceImpl<SpUserMapper, SpUser> implements SpUserService {

    @Autowired
    SpUserMapper spUserMapper;

    @Override
    public SpUser selectTest(Long id) {
        return spUserMapper.selectTest(id);
    }
}
