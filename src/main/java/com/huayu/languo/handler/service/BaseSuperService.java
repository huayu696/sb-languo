package com.huayu.languo.handler.service;

import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author liuxiongkang
 * @since 2019-05-22
 */
public interface BaseSuperService<T> extends IService<T> {

    /**
     * 批量插入
     *
     * @Date: 2019/5/27 15:15
     * @Param:
     * @Return:
     * @author: lxk
     */
    int insertBatch(List<T> entityList);
}
