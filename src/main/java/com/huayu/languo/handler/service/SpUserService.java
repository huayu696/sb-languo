package com.huayu.languo.handler.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huayu.languo.model.po.SpUser;

/**
 * <p>
 * sp用户注释 服务类
 * </p>
 *
 * @author liuxiongkang
 * @since 2019-05-09
 */
public interface SpUserService extends IService<SpUser> {

    SpUser selectTest(Long id);
}
