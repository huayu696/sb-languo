package com.huayu.languo.handler.service.impl;

import com.huayu.languo.handler.service.ProductRecInfoService;
import com.huayu.languo.mapper.ProductRecInfoMapper;
import com.huayu.languo.model.dto.ResultDto;
import com.huayu.languo.model.po.ProductConfig;
import com.huayu.languo.model.po.ProductRecInfo;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;


@Service("productRecInfoService")
@Slf4j
public class ProductRecInfoServiceImpl extends AbstractBaseSuperServiceImpl<ProductRecInfoMapper, ProductRecInfo> implements ProductRecInfoService {

    @Override
    public int insert(ProductRecInfo productRecInfo) throws Exception {
        return 0;
    }

    @Override
    public List<ProductRecInfo> selectWithQuery(ProductRecInfo productRecInfo) throws Exception {
        return null;
    }

    @Override
    public Integer countWithQuery(ProductRecInfo productRecInfo) throws Exception {
        return null;
    }

    @Override
    public ResultDto productRec(ProductRecInfo productRecInfo) throws Exception {
        return null;
    }

    @Override
    public ProductConfig insertAndReturn(ProductConfig productConfig, @NotBlank(message = "testEmail为空") @Email(message = "{test.email.error}") String testEmail, Date testDate,
                    @Range(message = "testRange错误", min = 1, max = 10) Integer testRange) {
        return null;
    }
}
