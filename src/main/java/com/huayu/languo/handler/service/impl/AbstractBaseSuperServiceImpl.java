package com.huayu.languo.handler.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huayu.languo.handler.service.BaseSuperService;
import com.huayu.languo.mapper.BaseSuperMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author liuxiongkang
 * @since 2019-05-22
 */
public abstract class AbstractBaseSuperServiceImpl<M extends BaseSuperMapper<T>, T> extends ServiceImpl<M, T> implements BaseSuperService<T> {

    @Autowired
    private BaseSuperMapper<T> baseSuperMapper;

    public AbstractBaseSuperServiceImpl() {
        //do noting
    }

    @Override
    public int insertBatch(List<T> entityList) {
        return baseSuperMapper.insertBatchSomeColumn(entityList);
    }
}
