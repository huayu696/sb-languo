package com.huayu.languo.handler.service;


import com.huayu.languo.model.dto.ResultDto;
import com.huayu.languo.model.po.ProductConfig;
import com.huayu.languo.model.po.ProductRecInfo;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.util.Date;
import java.util.List;

@Validated
public interface ProductRecInfoService {

    int insert(ProductRecInfo productRecInfo) throws Exception;

    List<ProductRecInfo> selectWithQuery(ProductRecInfo productRecInfo) throws Exception;

    Integer countWithQuery(ProductRecInfo productRecInfo) throws Exception;

    ResultDto productRec(ProductRecInfo productRecInfo) throws Exception;

    @NotNull
    public ProductConfig insertAndReturn(@NotNull ProductConfig productConfig, @NotBlank(message = "testEmail为空") @Email(message = "{test.email.error}") String testEmail,
                                         @NotNull(message = "testDate为空") @Past(message = "testDate错误") Date testDate,
                                         @NotNull(message = "testRange为空") @Range(message = "testRange错误", min = 1, max = 10) Integer testRange);


    // @CrossParameter
    // @CrossParameterScriptAssert(script = "args[0] == args[1]", lang = "javascript", alias = "args",
    // message = "changePassword{password.confirmation.error2}")
    // public void changePassword(String password, String confirmation);
}
