package com.huayu.languo.handler.redissub;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.stereotype.Component;


@Component
@Slf4j
public class RedisSubscribeConfig {

    @Autowired
    private LettuceConnectionFactory lettuceConnectionFactory;

    @Autowired
    private ChatMsgReceiver chatMsgReceiver;

    @Autowired
    private UserRegisterReceiver userRegisterReceiver;

    @Bean
    public ChannelTopic userRegisterTopic() {
        return new ChannelTopic("userRegister");
    }

    @Bean
    public ChannelTopic chatMsgTopic() {
        return new ChannelTopic("chatMsg");
    }

    // 监听关心的主题
    @Bean
    public RedisMessageListenerContainer setRedisMessageListenerContainer() {
        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        container.setConnectionFactory(lettuceConnectionFactory);
        // 主题1
        container.addMessageListener(userRegisterReceiver, userRegisterTopic());
        // 主题2
        container.addMessageListener(chatMsgReceiver, chatMsgTopic());
        return container;
    }
}
