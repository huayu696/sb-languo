package com.huayu.languo.handler.redissub;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Service;

@Service("userRegisterReceiver")
@Slf4j
public class UserRegisterReceiver implements MessageListener {


    private StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();

    @Override
    public void onMessage(Message message, byte[] pattern) {
        byte[] body = message.getBody();
        byte[] channel = message.getChannel();
        String msg = stringRedisSerializer.deserialize(body);
        String topic = stringRedisSerializer.deserialize(channel);
        log.info("userRegisterReceiver,监听:" + topic + ",收到消息：" + msg);
    }
}
