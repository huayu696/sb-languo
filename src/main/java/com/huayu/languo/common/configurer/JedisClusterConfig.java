package com.huayu.languo.common.configurer;//package com.huayu.languo.common.configurer;
//
//import java.util.HashSet;
//import java.util.Set;
//
//import javax.annotation.PostConstruct;
//
//import org.apache.commons.lang3.StringUtils;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//import com.cmb.qrcode.common.constant.AppConstants;
//import com.cmb.qrcode.common.util.AppProperties;
//
//import cn.hutool.core.util.StrUtil;
//import redis.clients.jedis.HostAndPort;
//import redis.clients.jedis.JedisCluster;
//import redis.clients.jedis.JedisPoolConfig;
//
///**
// * @author lxk
// */
//@Configuration
//public class JedisClusterConfig {
//
//    private static final int CONNECTION_TIMEOUT = 6000;
//
//    private static final int SO_TIMEOUT = 6000;
//
//    private static final int MAX_ATTEMPTS = 5;
//
//    private static final int MAX_TOTAL = 200;
//
//    private static final int MAX_IDLE = 50;
//
//    private static final int MIN_IDLE = 8;
//
//    private static final int MAX_WAIT_MILLIS = 3000;
//
//    private static final int SOFT_MIN_EVICTABLE_IDLE_TIME_MILLIS = 100;
//
//    private static final int TIME_BETWEEN_EVICTION_RUNS_MILLIS = 30000;
//
//    private static final int NUM_TESTS_PER_EVICTION_RUN = 10;
//
//    private static final int MIN_EVICTABLE_IDLE_TIME_MILLIS = 60000;
//
//    private static final Logger LOG = LoggerFactory.getLogger(JedisClusterConfig.class);
//
//    private static final String[] MONITOR_SERVERS = AppProperties.getSysEnvOrProp(AppConstants.MONITOR_REDIS_SERVER_HOST).split(AppConstants.DEFAULT_SPLIT_FLAG);
//
//    private static final String MONITOR_PASS_WORD = AppProperties.getSysEnvOrProp(AppConstants.MONITOR_REDIS_SERVER_CERT);
//
//    private static JedisCluster getJedisCluster(String[] serverArray, String passWord) {
//        Set<HostAndPort> nodes = new HashSet<>();
//        for (String ipPort : serverArray) {
//            String[] ipPortPair = StringUtils.split(ipPort, AppConstants.DEFAULT_CONCAT_FLAG);
//            nodes.add(new HostAndPort(ipPortPair[0].trim(), Integer.parseInt(ipPortPair[1].trim())));
//        }
//        if (StrUtil.isBlank(passWord)) {
//            return new JedisCluster(nodes, CONNECTION_TIMEOUT, SO_TIMEOUT, MAX_ATTEMPTS, jedisPoolConfig());
//        } else {
//            return new JedisCluster(nodes, CONNECTION_TIMEOUT, SO_TIMEOUT, MAX_ATTEMPTS, passWord, jedisPoolConfig());
//        }
//    }
//
//    private static JedisPoolConfig jedisPoolConfig() {
//        JedisPoolConfig config = new JedisPoolConfig();
//        // 最大连接数
//        config.setMaxTotal(MAX_TOTAL);
//        // 最大空闲连接数
//        config.setMaxIdle(MAX_IDLE);
//        // 设置最小空闲数
//        config.setMinIdle(MIN_IDLE);
//        // 获取连接时的最大等待毫秒数,小于零:阻塞不确定的时间,默认-1
//        config.setMaxWaitMillis(MAX_WAIT_MILLIS);
//        // 在获取连接的时候检查有效性, 默认false
//        config.setTestOnBorrow(true);
//        // 在归还给pool时，是否提前进行validate操作
//        config.setTestOnReturn(true);
//        // 在空闲时检查有效性, 默认false
//        config.setTestWhileIdle(true);
//        // 连接耗尽时是否阻塞, false报异常,ture阻塞直到超时, 默认true
//        config.setBlockWhenExhausted(true);
//        // 连接空闲多久后释放, 当空闲时间>该值 且 空闲连接>最大空闲连接数 时直接释放
//        config.setSoftMinEvictableIdleTimeMillis(SOFT_MIN_EVICTABLE_IDLE_TIME_MILLIS);
//        // Idle时进行连接扫描
//        config.setTestWhileIdle(true);
//        // 表示idle object evitor两次扫描之间要sleep的毫秒数----释放连接的扫描间隔（毫秒）
//        config.setTimeBetweenEvictionRunsMillis(TIME_BETWEEN_EVICTION_RUNS_MILLIS);
//        // 表示idle object evitor每次扫描的最多的对象数 -----每次释放连接的最大数目
//        config.setNumTestsPerEvictionRun(NUM_TESTS_PER_EVICTION_RUN);
//        // ------连接最小空闲时间
//        // 表示一个对象至少停留在idle状态的最短时间，然后才能被idle object evitor扫描并驱逐；这一项只有在timeBetweenEvictionRunsMillis大于0时才有意义
//        config.setMinEvictableIdleTimeMillis(MIN_EVICTABLE_IDLE_TIME_MILLIS);
//        return config;
//    }
//
//    @PostConstruct
//    public void configInfo() {
//        JedisCluster jedisCluster = jedisClusterMonitor();
//        LOG.info("加载 监控平台 redis server:[{}]", jedisCluster.getClusterNodes());
//    }
//
//    @Bean(destroyMethod = "close")
//    public JedisCluster jedisClusterMonitor() {
//        return getJedisCluster(MONITOR_SERVERS, MONITOR_PASS_WORD);
//    }
//
//}
