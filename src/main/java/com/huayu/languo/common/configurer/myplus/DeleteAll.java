package com.huayu.languo.common.configurer.myplus;

import com.baomidou.mybatisplus.core.injector.AbstractMethod;
import com.baomidou.mybatisplus.core.metadata.TableInfo;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlSource;

/**
 * 删除所有
 */
public class DeleteAll extends AbstractMethod {

    @Override
    public MappedStatement injectMappedStatement(Class<?> mapperClass, Class<?> modelClass, TableInfo tableInfo) {
        String sql;
        String sqlTemp = "<script>\nDELETE FROM %s %s\n</script>";
        if (tableInfo.isLogicDelete()) {
            sql = String.format(sqlTemp, tableInfo.getTableName(), tableInfo, sqlWhereEntityWrapper(true, tableInfo));
        } else {
            sql = String.format(sqlTemp, tableInfo.getTableName(), sqlWhereEntityWrapper(true, tableInfo));
        }
        SqlSource sqlSource = languageDriver.createSqlSource(configuration, sql, modelClass);
        return addUpdateMappedStatement(mapperClass, modelClass, "deleteAll", sqlSource);
    }
}
