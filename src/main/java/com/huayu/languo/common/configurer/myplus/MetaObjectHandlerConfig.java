package com.huayu.languo.common.configurer.myplus;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Date;

@Component
public class MetaObjectHandlerConfig implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        System.out.println("插入方法实体填充");

        Object inTimeVal = getFieldValByName("inTime", metaObject);
        if (inTimeVal == null) {
            setFieldValByName("inTime", new Date(), metaObject);
        }

        Object updTimeVal = getFieldValByName("updTime", metaObject);
        if (updTimeVal == null) {
            setFieldValByName("updTime", new Date(), metaObject);
        }

        Object versionVal = getFieldValByName("version", metaObject);
        if (versionVal == null) {
            setFieldValByName("version", 0, metaObject);
        }

        Object delFlagVal = getFieldValByName("delFlag", metaObject);
        if (delFlagVal == null) {
            setFieldValByName("delFlag", 0, metaObject);
        }
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        System.out.println("更新方法 实体填充 ");
        Object updTimeVal = getFieldValByName("updTime", metaObject);
        if (updTimeVal == null) {
            setFieldValByName("updTime", new Date(), metaObject);
        }
    }
}
