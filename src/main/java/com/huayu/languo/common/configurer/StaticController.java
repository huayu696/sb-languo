package com.huayu.languo.common.configurer;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class StaticController extends WebMvcConfigurerAdapter {

    // https://baijiahao.baidu.com/s?id=1569668606373509&wfr=spider&for=pc
    // 方案1、默认采用springboot静态资源路径在src/main/resources创建/static或/public或/resources或/META-INF/resources可以直接访问静态资源，默认会放到classpath目录中
    // 方案2、通过application.properties配置spring.resources.static-locations=classpath:/img/指定自定义静态文件的目录位置，，多个使用逗号分隔，springboot自动失效
    // 方案3、创建StaticController类继承WebMvcConfigurerAdapter 重写addResourceHandlers
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
        // registry.addResourceHandler("*.css").addResourceLocations("classpath:/static/css/");
        super.addResourceHandlers(registry);
    }
}
