package com.huayu.languo.common.configurer;

import com.huayu.languo.common.constant.AppConstants;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Swagger配置文件。 [Springfox官方集成文档](http://springfox.github.io/springfox/docs/current/)
 * [Swagger注解官方文档](https://github.com/swagger-api/swagger-core/wiki/Annotations-1.5.X)
 * 
 * @Description:
 * @Author: lxk
 * @Date: 2019-3-10 2:00
 */
@Configuration
//@EnableWebMvc 只能添加到一个@Configuration配置类上，用于导入Spring Web MVC configuration
//@EnableWebMvc
@EnableSwagger2
public class Swagger2Config {

    private static final String VERSION = "1.0.0";

    private static final String LICENSE_URL = "http://www.cmbchina.com";

    /** * 根据配置读取是否开启swagger文档，针对测试与生产环境采用不同的配置 */
    @Value("${swagger.enable}")
    private boolean isSwaggerEnable;

    public Swagger2Config() {
        // do nothing
    }

    ApiInfo webApiInfo() {
        return new ApiInfoBuilder().
        // 设置文档的标题
                        title("招行LV65GM 理财产品录入项目 对外接口")
                        // 设置文档的描述->1.Overview
                        .description("更多内容请关注：http://www.cmbchina.com")
                        // 设置文档的License信息->1.3
                        .license("招行LV65GM").licenseUrl(LICENSE_URL)
                        // 服务条款
                        .termsOfServiceUrl(LICENSE_URL)
                        // 设置文档的版本信息-> 1.1 Version information
                        .version(VERSION)
                        // 设置文档的联系方式->1.2 Contact information
                        .contact(new Contact("liuxiongkang", LICENSE_URL, "liuxiongkang@gmail.com")).build();
    }

    ApiInfo adminApiInfo() {
        return new ApiInfoBuilder().
        // 设置文档的标题
                        title("招行LV65GM 理财产品录入项目 对内接口")
                        // 设置文档的描述->1.Overview
                        .description("更多内容请关注：http://www.cmbchina.com")
                        // 设置文档的License信息->1.3
                        .license("招行LV65GM").licenseUrl(LICENSE_URL)
                        // 服务条款 .termsOfServiceUrl(LICENSE_URL)
                        // 设置文档的版本信息-> 1.1 Version information
                        .version(VERSION)
                        // 设置文档的联系方式->1.2 Contact information
                        .contact(new Contact("liuxiongkang", LICENSE_URL, "liuxiongkang@gmail.com")).build();
    }

    @Bean
    public Docket webApi() {
        return new Docket(DocumentationType.SWAGGER_2).groupName("app-web").enable(isSwaggerEnable).apiInfo(webApiInfo()).select()
                        // 对所有该包下的Api进行监控，如果想要监控所有的话可以改成any()
                        .apis(RequestHandlerSelectors.basePackage(AppConstants.SWAGGER_SCAN_BASE_PACKAGE))
                        // 对所有路径进行扫描,或者配置请求路径正则
                        // .paths(regex("/product.*"))
                        // 匹配任何路径
                        .paths(PathSelectors.any()).paths(PathSelectors.ant("/app/**")).build().directModelSubstitute(org.joda.time.LocalDate.class, java.sql.Date.class)
                        .directModelSubstitute(org.joda.time.DateTime.class, java.util.Date.class);
    }

    @Bean
    public Docket adminApi() {
        return new Docket(DocumentationType.SWAGGER_2).groupName("app-admin").enable(isSwaggerEnable).apiInfo(adminApiInfo()).select()
                        // 对所有该包下的Api进行监控，如果想要监控所有的话可以改成any()
                        .apis(RequestHandlerSelectors.basePackage(AppConstants.SWAGGER_SCAN_BASE_PACKAGE))
                        // 对所有路径进行扫描,或者配置请求路径正则
                        // .paths(regex("/product.*"))
                        // 匹配任何路径
                        .paths(PathSelectors.any()).paths(PathSelectors.ant("/admin/**")).build().directModelSubstitute(org.joda.time.LocalDate.class, java.sql.Date.class)
                        .directModelSubstitute(org.joda.time.DateTime.class, java.util.Date.class);
    }


    //可以注入多个doket，也就是多个版本的api，可以在看到有三个版本groupName不能是重复的，v1和v2是ant风格匹配，配置文件
    //@Bean
    //public Docket api() {
    //    //可以添加多个header或参数
    //    ParameterBuilder aParameterBuilder = new ParameterBuilder();
    //    aParameterBuilder.parameterType("header") //参数类型支持header, cookie, body, query etc
    //                    .name("token") //参数名
    //                    .defaultValue("token") //默认值
    //                    .description("header中token字段测试").modelRef(new ModelRef("string"))//指定参数值的类型
    //                    .required(false).build(); //非必需，这里是全局配置，然而在登陆的时候是不用验证的
    //    List<Parameter> aParameters = new ArrayList<Parameter>();
    //    aParameters.add(aParameterBuilder.build());
    //    return new Docket(DocumentationType.SWAGGER_2).groupName("v1").select().apis(RequestHandlerSelectors.any()).paths(PathSelectors.ant("/api/v1/**")).build()
    //                    .apiInfo(apiInfo1()).globalOperationParameters(aParameters);
    //}


    private ApiInfo apiInfo1() {
        return new ApiInfoBuilder().title("exampleApi 0.01").termsOfServiceUrl("www.example.com")
                        .contact(new Contact("liumei", "http://blog.csdn.net/pc_gad", "hilin2333@gmail.com")).version("v0.01").build();
    }
}
