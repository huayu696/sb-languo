package com.huayu.languo.common.configurer;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.serializer.RedisSerializer;

@Configuration
public class RedisSessionSerializerConfig {

    @Bean("springSessionDefaultRedisSerializer")
    public RedisSerializer setDefaultRedisSerializer(RedisSerializer fastRedisSerializer) {
        return fastRedisSerializer;
    }
}
