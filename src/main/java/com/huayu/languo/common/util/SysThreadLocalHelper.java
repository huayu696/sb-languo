package com.huayu.languo.common.util;

/**
 * @author 刘雄康
 * @version v1.0
 * @description 日志记录
 * @date 2019年03月15日
 */
public final class SysThreadLocalHelper {

    public static final ThreadLocal<Long> REQ_START_TIME = new ThreadLocal<>();

    private SysThreadLocalHelper() {
        super();
    }
}
