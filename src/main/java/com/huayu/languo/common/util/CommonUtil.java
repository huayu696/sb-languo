package com.huayu.languo.common.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.huayu.languo.common.constant.AppConstants;
import com.huayu.languo.model.excp.SystemException;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Connection;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author 刘雄康
 * @version v1.0
 * @description 常用方法提取
 * @date 2019年03月27日
 */
public final class CommonUtil {

    private CommonUtil() {
        super();
    }

    public static boolean canPraseHttpBody(Connection.Response response) {
        return canParseHttpBody(response.statusCode(), response.statusMessage(), response.body());
    }

    public static boolean canParseHttpBody(int statusCode, String statusMsg, String body) {
        return statusCode == AppConstants.HTTP_SUCC_STATUS_CODE && StringUtils.equalsIgnoreCase(AppConstants.MSG_OK, statusMsg) && StringUtils.isNotEmpty(body);
    }

    public static boolean canPraseHttpBody(int statusCode, String statusMsg) {
        return statusCode == AppConstants.HTTP_SUCC_STATUS_CODE && StringUtils.equalsIgnoreCase(AppConstants.MSG_OK, statusMsg);
    }

    public static boolean checkTimeBetween(Date startTime, Date midTime, Date endTime) {
        if (midTime == null || startTime == null || endTime == null) {
            throw new SystemException("time 不能为空!");
        }
        boolean flag = true;
        if (midTime.before(startTime)) {
            flag = false;
        }
        if (endTime.before(midTime)) {
            flag = false;
        }
        return flag;
    }

    public static String trim(String body) {
        if (null == body) {
            return null;
        }
        return body.trim();
    }

    public static String toStr(Object data) {
        if (null == data) {
            return "";
        }
        return data.toString().trim();
    }

    public static String toJsonStr(Object data) {
        if (null == data) {
            return "";
        }
        try {
            return new ObjectMapper().writeValueAsString(data);
        } catch (JsonProcessingException e) {
            throw new SystemException(e);
        }
    }

    public static boolean areEmpty(Collection collection) {
        return null == collection || collection.isEmpty();
    }

    public static boolean areEmpty(Map<String, String> map) {
        return null == map || map.isEmpty();
    }

    public static String createUrlParam(Map<String, String> params) {
        if (areEmpty(params)) {
            return AppConstants.DEFAULT_STR_VAL;
        }
        List<String> list = new ArrayList<>();
        for (Map.Entry<String, String> param : params.entrySet()) {
            list.add(param.getKey().concat(AppConstants.EQUALS_STR).concat(param.getValue()));
        }
        return StringUtils.join(list, AppConstants.URL_PARAM_CONCAT_FLAG);
    }

    public static String concatUrlParam(String url, String urlParams) {
        if (url.contains(AppConstants.QUERY_STR)) {
            return url.concat(AppConstants.URL_PARAM_CONCAT_FLAG).concat(urlParams);
        }
        return url.concat(AppConstants.QUERY_STR).concat(urlParams);
    }
}
