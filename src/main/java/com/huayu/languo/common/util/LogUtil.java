package com.huayu.languo.common.util;

import com.huayu.languo.common.constant.AppConstants;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StreamUtils;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

/**
 * @author 刘雄康
 * @version v1.0
 * @description 日志记录
 * @date 2019年03月15日
 */
public final class LogUtil {

    private static final Logger LOG = LoggerFactory.getLogger(LogUtil.class);

    private LogUtil() {
        super();
    }

    public static String getReqInfo(HttpServletRequest request, Long costTime) {
        StringBuffer msg = new StringBuffer();
        msg.append("HAVE A SLOW REQUEST【".concat(CommonUtil.toStr(costTime)).concat("ms】 "));
        msg.append(" reqUrl [".concat(getReqUrl(request)).concat("] "));
        msg.append(" , reqMethod [".concat(request.getMethod().concat("] ")));
        msg.append(" , reqHeard [".concat(getHeaderStr(request)).concat("] "));
        msg.append(" , reqBody [".concat(getReqBody(request)).concat("] "));
        return msg.toString();
    }

    private static String getHeaderStr(HttpServletRequest request) {
        Enumeration<String> headerNames = request.getHeaderNames();
        List<String> list = new ArrayList<>();
        while (headerNames.hasMoreElements()) {
            String headerName = headerNames.nextElement();
            list.add(headerName.concat(AppConstants.DEFAULT_CONCAT_FLAG).concat(CommonUtil.toStr(request.getHeader(headerName))));
        }
        return String.join(AppConstants.DEFAULT_STR_END, list);
    }

    public static Long getReqCostTime(Long endTime) {
        Long startTime = SysThreadLocalHelper.REQ_START_TIME.get();
        if (null == startTime) {
            return 0L;
        }
        SysThreadLocalHelper.REQ_START_TIME.remove();
        return endTime - startTime;
    }

    public static Long getReqCostTime() {
        return getReqCostTime(System.currentTimeMillis());
    }

    public static void setReqStartTime(Long startTime) {
        SysThreadLocalHelper.REQ_START_TIME.set(startTime);
    }

    public static String getReqBody(ServletRequest request) {
        if (request instanceof MultipartHttpServletRequest || request.getContentType().contains("multipart")) {
            return "may upload file body....!";
        }
        String body = null;
        try {
            body = StreamUtils.copyToString(request.getInputStream(), Charset.forName("utf-8"));
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
        }
        return CommonUtil.toStr(body);
    }

    public static String getReqUrl(HttpServletRequest request) {
        if (StringUtils.isNotEmpty(request.getQueryString())) {
            return request.getRequestURL().toString().concat("?").concat(request.getQueryString());
        }
        return request.getRequestURL().toString();
    }
}
