package com.huayu.languo.common.util;

import cn.hutool.core.util.CharsetUtil;
import cn.hutool.setting.dialect.Props;
import org.apache.commons.lang3.StringUtils;

/**
 * @author 刘雄康
 * @version v1.0
 * @description AppProperties
 * @date 2019年03月09日
 */
public final class AppProperties {

    private static Props props = new Props("customize.properties", CharsetUtil.UTF_8);

    private AppProperties() {
    }


    public static String getAppPro(String key) {
        return props.getStr(key);
    }

    public static String getEnvKV(String key) {
        return System.getenv(key);
    }

    public static String getSysEnvOrProp(String key, String defaultVal) {
        String env = getEnvKV(key);
        if (StringUtils.isEmpty(env)) {
            String appPro = getAppPro(key);
            if (StringUtils.isEmpty(appPro)) {
                return defaultVal;
            }
            return appPro;
        }
        return env;
    }

    /**
     * 从本地环境变量取值，取不到则从配置文件
     */
    public static String getSysEnvOrProp(String key) {
        return getSysEnvOrProp(key, null);
    }
}
