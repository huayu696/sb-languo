package com.huayu.languo.common.util;

import java.util.Map;

public abstract class AbstractTask implements Runnable {

    /** 上下文参数 */
    private Map<String, Object> context;

    /**
     * 获取上下文参数
     * 
     * @return context
     */
    public Map<String, Object> getContext() {
        return context;
    }

    /**
     * 设置上下文参数
     * 
     * @param context 要设置的上下文参数
     */
    public void setContext(Map<String, Object> context) {
        this.context = context;
    }
}
