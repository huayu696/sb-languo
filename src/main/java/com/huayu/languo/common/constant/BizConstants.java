package com.huayu.languo.common.constant;

/**
 * @author 刘雄康
 * @version v1.0
 * @description  第三方接口等业务常量
 * @date 2019年03月21日
 */
public final class BizConstants {

    /////////////////////////////////////////// 第三方接口等 ///////////////////////////////////////////
    public static final String MONITOR_URL_REGISTER_URL = "third.service.monitor.url.register.url";

    public static final String QR_PLAT_LINK_TRANS_URL_BATCH = "third.service.qr.plat.link.trans.url.batch";

    public static final String QR_PLAT_LINK_TRANS_URL_SINGLE = "third.service.qr.plat.link.trans.url.single";

    public static final String QR_PLAT_UPD_CONFIG_URL = "third.service.qr.plat.upd.config.url";

    public static final String QR_PLAT_UPD_CONFIG_CALLBACK_URL = "third.service.qr.plat.upd.config.callBack.url";

    public static final String QR_PLAT_PRO_INFO_URL = "third.service.qr.plat.pro.info.url";

    public static final String QR_PLAT_CHAN_INFO_URL = "third.service.qr.plat.chan.info.url";

    ///////////////////////////////////////////  业务数据 key定义等 ///////////////////////////////////////////
    public static final String MONITOR_URL_KEY = "qr:monitor:url:";

    public static final int MONITOR_URL_KEY_TIME = 30 * 24 * 60 * 60;

    public static final String INCR_REC_CUS = "qr:incr:rec:cus";

    public static final String QR_MONITOR_CHAN_CODE = "{qr:monitor:chan:code}:";

    public static final int QR_MONITOR_CHAN_CODE_TIME = 90 * 24 * 60 * 60;

    private BizConstants() {
        super();
    }
}
