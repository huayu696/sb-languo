package com.huayu.languo.common.constant;

/**
 * @author 刘雄康
 * @version v1.0
 * @description hibernate 校验message 
 * @date 2019年03月21日
 */
public final class ValidMsg {

    /////////////////////////////////////////// 项目基本常量///////////////////////////////////////////
    public static final String PAGE_CURRENT_MSG_DEFAULT = "current需在0-10000之间";

    public static final String PAGE_SIZE_MSG_DEFAULT = "size需在0-10000之间";

    private ValidMsg() {
        super();
    }
}
