package com.huayu.languo.common.constant;

/**
 * @author  LXK
 * 响应体增强 排序
 * 
 */
public final class OrderConstant {

    public static final int JSONP_ORDER = 8;

    public static final int GLOBAL_ERROR_ORDER = Integer.MIN_VALUE;

    public static final int API_GLOBAL_RESPONSE_ORDER = 10;

}
