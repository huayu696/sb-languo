package com.huayu.languo.common.constant;

/**
 * 项目常量
 */
public final class AppConstants {
    /////////////////////////////////////////// 项目基本常量///////////////////////////////////////////
    /**
     * 项目基础包名称，根据自己公司的项目修改
     */
    public static final String BASE_PACKAGE = "com.huayu.languo";

    public static final String SWAGGER_SCAN_BASE_PACKAGE = "cmb.huayu.languo.controller";

    public static final String DEFAULT_SPLIT_FLAG = ",";

    public static final String DEFAULT_STR_END = ";";

    public static final String DEFAULT_CONCAT_FLAG = ":";

    public static final String EQUALS_STR = "=";

    public static final String QUERY_STR = "?";

    public static final String MSG_PLACE = "{}";

    public static final String STR_FORMAT_PLACE = "%s";

    public static final String URL_PARAM_CONCAT_FLAG = "&";

    public static final String DEFAULT_STR_VAL = "";

    public static final Integer HTTP_SUCC_STATUS_CODE = 200;

    public static final Integer INT1 = 1;

    public static final Integer INT0 = 0;

    public static final Integer INT2 = 2;

    public static final Integer INT16 = 16;

    public static final String STR1 = "1";

    public static final String NULL = null;

    public static final String MSG_OK = "OK";

    public static final String FALSE_STR = "false";

    public static final String HIBERNATE_VALID_FAILFAST = "hibernate.valid.failFast";

    public static final String NEED_FILTER_ORIGIN = "need.filter.origin";

    public static final String ALLOW_ORIGIN = "app.allow.origin";

    ///////////////////////////////////////////  基础服务配置常量///////////////////////////////////////////
    public static final String MONITOR_REDIS_SERVER_HOST = "monitor.redis.server.ipports";

    public static final String MONITOR_REDIS_SERVER_CERT = "monitor.redis.server.password";


    private AppConstants() {
        super();
    }
}

