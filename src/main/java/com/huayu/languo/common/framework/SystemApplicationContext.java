package com.huayu.languo.common.framework;


import org.springframework.context.ApplicationContext;

public final class SystemApplicationContext {

    private static ApplicationContext ctx;

    private SystemApplicationContext() {
        //do nothing
    }

    public static void init(ApplicationContext context) {
        ctx = context;
    }

    public static Object getBean(String beanName) {
        if (ctx == null) {
            return null;
        }
        return ctx.getBean(beanName);
    }

    public static String[] getBeanNameOfType(Class<?> clazz) {
        return ctx.getBeanNamesForType(clazz);
    }

    public static boolean containsBean(String beanName) {
        return ctx.containsBean(beanName);
    }
}
