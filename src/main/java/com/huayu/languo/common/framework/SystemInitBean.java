package com.huayu.languo.common.framework;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.support.ApplicationObjectSupport;
import org.springframework.stereotype.Component;

@Component
public class SystemInitBean extends ApplicationObjectSupport implements InitializingBean {

    private static final Logger LOG = LoggerFactory.getLogger(SystemInitBean.class);

    private static void initSystemTask() {
        //启动定时任务
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        LOG.info("加载 SystemInitBean ............");

        // 1 注入管理的bean
        SystemApplicationContext.init(getApplicationContext());
        // 2 加载系统基础组件
        // 3 初始化 DFA 敏感字库
        // 4 初始化系统后台定时任务
        initSystemTask();
    }
}
