package com.huayu.languo.common.framework;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class WebSecurityConfig extends WebMvcConfigurerAdapter {

    @Bean
    public AppAutoInterceptor getAppInterceptor() {
        return new AppAutoInterceptor();
    }

    @Bean
    public AdminAuthInterceptor getAdminInterceptor() {
        return new AdminAuthInterceptor();
    }

    @Bean
    public ApiAuthInterceptor getApiInterceptor() {
        return new ApiAuthInterceptor();
    }


    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 所有已api开头的访问都要进入RedisSessionInterceptor拦截器进行登录验证，并排除login接口(全路径)。必须写成链式，分别设置的话会创建多个拦截器。
        // 必须写成getSessionInterceptor()，否则SessionInterceptor中的@Autowired会无效
        registry.addInterceptor(getAppInterceptor()).addPathPatterns("/app/**").excludePathPatterns("/app/user/login");
        registry.addInterceptor(getAdminInterceptor()).addPathPatterns("/admin/**").excludePathPatterns("/admin/user/login");
        registry.addInterceptor(getApiInterceptor()).addPathPatterns("/api/**");
        // 是否除了 app，admin ，api 以外就不保护其他前缀了
        //registry.addInterceptor(getApiInterceptor()).addPathPatterns("/api/**");
        super.addInterceptors(registry);
    }
}
