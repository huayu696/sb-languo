package com.huayu.languo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.sql.SQLException;

@SpringBootApplication
@EnableTransactionManagement
@EnableScheduling
@EnableAsync
@Slf4j
public class Application {

    public static void main(String[] args) throws SQLException {
        SpringApplication.run(Application.class, args);
        //ConfigurableApplicationContext context = SpringApplication.run(Application.class, args);
        //DataSource ds = context.getBean(DataSource.class);
        //System.out.println("》》》》》 " + ds.getClass().getName()); //默认的使用的是tomcat的数据源
        //Connection connection = ds.getConnection();
        //System.out.println("》》》》》 " + connection.getCatalog()); //数据库名
        //System.out.println("》》》》》 " + context.getBean(JdbcTemplate.class));
        //connection.close();
        log.warn("线程:" + Thread.currentThread().getName());
    }
}

