package com.huayu.languo.model.po;


import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ApiModel(value = "ProductRecInfo-DTO", description = "理财意愿者推荐信息")
public class ProductRecInfo extends AbstractSuperEntity{

    @ApiModelProperty(value = "招行内部uid", hidden = true, example = "uid1234")
    private String uid;

    @ApiModelProperty(value = "产品编号", hidden = true, example = "prodNo1234")
    private String prodNo;


    @ApiModelProperty(value = "交易时间", required = true, example = "2019-03-09 02:03:04")
    private String transTime;

    @ApiModelProperty(value = "活动编号", required = true, example = "actNo1234")
    private String actNo;

    @ApiModelProperty(value = "用户姓名", required = false, example = "张三")
    private String custName;

    @ApiModelProperty(value = "渠道编号", allowableValues = "range[1,9]", required = true, example = "8")
    private Integer channel;

    @ApiModelProperty(value = "渠道流水号", required = true, example = "channelRec00011")
    private String transNo;

    @ApiModelProperty(value = "电话号码", required = true, example = "13530344043")
    private String tel;

}
