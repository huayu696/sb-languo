package com.huayu.languo.model.po;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 测试自定义实体父类 ， 这里可以放一些公共字段信息
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "基础对象", description = "基础对象")
public abstract class AbstractSuperEntity implements Serializable {


    @ApiModelProperty(value = "主键自增")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private static final long serialVersionUID = 1L;

    /**
     * 入库时间
     */
    @TableField(value = "in_time", fill = FieldFill.INSERT)
    private Date inTime;

    /**
     * 更新时间
     */
    @TableField(value = "upd_time", fill = FieldFill.INSERT_UPDATE)
    private Date updTime;


}
