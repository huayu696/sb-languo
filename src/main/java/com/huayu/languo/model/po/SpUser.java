package com.huayu.languo.model.po;

import com.alibaba.fastjson.annotation.JSONField;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.Version;
import com.huayu.languo.model.enums.SexEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
* @description  sp用户注释
* @date     2019-05-09
* @author   liuxiongkang
* @version  v1.0
*/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "SpUser对象", description = "sp用户注释")
public class SpUser extends AbstractSuperEntity implements Serializable {

    private static final long serialVersionUID = 313680792622149041L;

    @ApiModelProperty(value = "姓名")
    @TableField("username")
    private String username;

    @ApiModelProperty(value = "密码")
    @TableField("password")
    private String password;

    @ApiModelProperty(value = "昵称")
    @TableField("nick_name")
    private String nickName;

    @ApiModelProperty(value = "性别")
    @TableField("sex")
    @JSONField(serialzeFeatures = SerializerFeature.WriteEnumUsingToString)
    private SexEnum sex;

    @ApiModelProperty(value = "版本")
    @TableField("version")
    @Version
    private Integer version;

    @ApiModelProperty(value = "删除状态")
    @TableField("del_flag")
    @TableLogic
    private Integer delFlag;

}
