package com.huayu.languo.model.po;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class JobEntity extends AbstractSuperEntity implements Serializable {

    @TableField("`name`")
    private String name; //job名称

    @TableField("`group`")
    private String group; //job组名

    private String cron; //执行的cron

    private String parameter; //job的参数

    private String description; //job描述信息

    private String vmParam; //vm参数

    private String jarPath; //job的jar路径,在这里我选择的是定时执行一些可执行的jar包

    @TableField("status")
    private String status; //job的执行状态,这里我设置为OPEN/CLOSE且只有该值为OPEN才会执行该Job
}
