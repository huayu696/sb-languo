package com.huayu.languo.model.po;


import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductConfig extends AbstractSuperEntity{

    @ApiModelProperty(value = "渠道编号", allowableValues = "range[1,9]", required = true, example = "8")
    private Integer channel;

    @ApiModelProperty(value = "活动编号", required = true, example = "actNo0001")
    private String actNo;

    @ApiModelProperty(value = "产品编号", required = true, example = "prodNo0001")
    private String prodNo;

    @ApiModelProperty(value = "产品名称", required = true, example = "prodName0001")
    private String prodName;

    @ApiModelProperty(value = "产品开始有效时间", hidden = true, example = "2018-01-01 02:03:04")
    private String stTime;

    @ApiModelProperty(value = "产品开始无效时间", hidden = true, example = "2019-03-09 02:03:04")
    private String edTime;

    @ApiModelProperty(value = "产品创建人", required = true, example = "creater0001")
    private String creater;

    @ApiModelProperty(value = "推送状态", hidden = true, example = "0,1")
    private Integer statusFlag;
}
