package com.huayu.languo.model.excp;


import com.huayu.languo.model.enums.ResultCode;

/**
 * @author 刘雄康
 * @version v1.0
 * @description 系统业务异常类
 * @date 2019年04月03日
 */
public class BizException extends AbstractAppException {

    private static final long serialVersionUID = 194906846739586856L;

    public BizException(ResultCode resultCode) {
        super(resultCode);
    }

    public BizException(ResultCode resultCode, Object data) {
        super(resultCode, data);
    }

    public BizException(ResultCode resultCode, Object data, String msg) {
        super(resultCode, data, msg);
    }

    public BizException(ResultCode resultCode, Object data, String formatMsg, Object... objects) {
        super(resultCode, data, formatMsg, objects);
    }
}
