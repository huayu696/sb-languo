package com.huayu.languo.model.excp;

import com.huayu.languo.common.constant.AppConstants;
import org.apache.commons.lang3.StringUtils;


/**
 * @author 刘雄康
 * @version v1.0
 * @description 系统异常包装类
 * @date 2019年04月03日
 */
public class SystemException extends AbstractAppException {

    public SystemException(Exception e) {
        super(e, e.getMessage());
        String debugMsg = AppConstants.DEFAULT_STR_VAL;
        if (StringUtils.isEmpty(super.getDebugMessage())) {
            debugMsg = super.getDebugMessage();
        }
        super.setDebugMessage(e.getClass().getName().concat(AppConstants.DEFAULT_STR_END).concat(debugMsg));
    }

    public SystemException(String msg) {
        super(msg, false);
    }
}
