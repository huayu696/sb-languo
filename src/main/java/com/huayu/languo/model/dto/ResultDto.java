package com.huayu.languo.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.huayu.languo.model.enums.ResultCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * @author 刘雄康
 * @version v1.0
 * @description 响应结果对象
 * @date 2019年03月08日
 */
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@Builder
@ApiModel(value = "Result-DTO", description = "API接口返回对象")
public class ResultDto implements Serializable {
    public static final String MSG_PLACE = "{}";

    public static final String REPLACEMENT = "%s";

    private static final long serialVersionUID = 85475141232661244L;

    @ApiModelProperty(value = "请求状态码", required = true, example = "MGM0000")
    private String code;

    @ApiModelProperty(value = "请求状态码对应汉字描述", required = true, example = "操作成功")
    private String msg;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(value = "具体业务接口返回值(returnCode= SUCCESS 时赋值)", example = "{\"cmdUser\": 1 , \"otherKey\" : \"otherValue\"}")
    private transient Object data;

    public static ResultDto success() {
        ResultDto result = new ResultDto();
        result.setResultCode(ResultCode.SUCCESS);
        return result;
    }

    public static ResultDto success(Object data, Object defaultData) {
        if (null == data) {
            data = defaultData;
        }
        return success(data);
    }

    public static ResultDto success(Object data) {
        ResultDto result = success();
        result.setData(data);
        return result;
    }

    public static ResultDto failure(ResultCode resultCode) {
        ResultDto result = new ResultDto();
        result.setResultCode(resultCode);
        return result;
    }

    public static ResultDto failure(ResultCode resultCode, Object data) {
        ResultDto result = failure(resultCode);
        result.setData(data);
        return result;
    }

    public static ResultDto failure(ResultCode resultCode, Object data, String msgDesc) {
        ResultDto result = failure(resultCode, data);
        result.setMsg(String.format(result.getMsg().replace(MSG_PLACE, REPLACEMENT), msgDesc));
        return result;
    }

    public static ResultDto failure(ResultCode resultCode, Object data, String msgDesc, List<String> msgDescFormat) {
        ResultDto result = failure(resultCode, data);
        result.setMsg(String.format(msgDesc.replace(MSG_PLACE, REPLACEMENT), msgDescFormat));
        return result;
    }

    public static ResultDto create() {
        return new ResultDto();
    }

    public ResultDto setResultCode(ResultCode code) {
        this.code = code.code();
        this.msg = code.message();
        return this;
    }

    public ResultDto setRespMsgCtThis(String msgDesc) {
        this.msg = String.format(msg.replace(MSG_PLACE, "%s"), msgDesc);
        return this;
    }
}
