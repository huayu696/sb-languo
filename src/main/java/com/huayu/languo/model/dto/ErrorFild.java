package com.huayu.languo.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ErrorFild {

    private String field;

    private String message;

    private Object value;

    @Override
    public String toString() {
        return "{" + "field='" + field + '\'' + ", message='" + message + '\'' + ", value='" + value + "'}";
    }
}
