package com.huayu.languo.model.dto;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

/**  校验结果类
     */
@Data
public class ValidResult {

    /**是否有错误
     */
    private boolean hasErrors;

    /**错误信息
     */
    private List<ErrorFild> errors;

    public ValidResult() {
        this.errors = new ArrayList<>();
    }

    public boolean hasErrors() {
        return hasErrors;
    }

    public void setHasErrors(boolean hasErrors) {
        this.hasErrors = hasErrors;
    }

    /**
     * 获取所有验证信息
     * @return 字符串形式
     */
    public String getErrors() {
        return errors.toString();
    }

    public void addError(String propertyName, String message, Object validVal) {
        this.errors.add(new ErrorFild(propertyName, message, validVal));
    }
}
