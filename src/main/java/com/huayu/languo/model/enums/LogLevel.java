package com.huayu.languo.model.enums;

public enum LogLevel {
    DEBUG("全量跟踪日志"), INFO("普通日志"), WARN("警告日志"), ERROR("异常日志");

    private String level;

    LogLevel(String level) {
        this.level = level;
    }
}
