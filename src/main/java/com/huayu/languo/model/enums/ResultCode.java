package com.huayu.languo.model.enums;


import com.baomidou.mybatisplus.core.enums.IEnum;

public enum ResultCode  implements IEnum<String> {
    // 200 成功
    SUCCESS("200", "操作成功"),
    // 400（错误请求） 服务器不理解请求的语法。get post 或者要求json 等等不一致导致
    BAD_REQUEST_400("400", "请求语法异常[{}]"),
    // 401 （未授权） 请求要求身份验证。 对于需要登录的网页，服务器可能返回此响应。 白名单权限，域名权限，AK 帐号密码 验证失败 等等
    UNAUTHORIZED_401("401", "权限校验异常[{}]"),
    // 404 接口不存在，或接口无访问权限
    NOT_FOUND_404("404", "接口不存在或受限[{}]"),
    // 417 （未满足期望值） 服务器未满足"期望" 请求标头字段的要求
    PARAMS_VALIDATE_412("412", "业务参数异常[{}]"),
    // 420 业务场景:活动已下线；此活动不支持多次参与等等
    BIZ_FAIL_420("420", "业务场景异常[{}]"),
    // 500 服务器内部错误
    SERVER_ERROR_500("500", "系统异常");

    private String code;

    private String message;

    ResultCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public static ResultCode getByCode(Integer code) {
        if (code == null) {
            return null;
        }
        for (ResultCode exceptionEnum : ResultCode.values()) {
            if (code.equals(exceptionEnum.code)) {
                return exceptionEnum;
            }
        }
        return null;
    }

    public String code() {
        return code;
    }

    public String message() {
        return message;
    }

    public ResultCode setCode(String code) {
        this.code = code;
        return this;
    }

    public ResultCode setMessage(String message) {
        this.message = message;
        return this;
    }

    /**
     * 枚举数据库存储值
     */
    @Override
    public String getValue() {
        return code;
    }
}
