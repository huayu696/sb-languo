package com.huayu.languo.mapper;

import com.huayu.languo.model.po.JobEntity;

/**
 * Created by EalenXie on 2018/6/4 14:27
 */
public interface JobEntityMapper extends BaseSuperMapper<JobEntity> {

}
