package com.huayu.languo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huayu.languo.model.po.SpUser;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * sp用户注释 Mapper 接口
 * </p>
 *
 * @author liuxiongkang
 * @since 2019-05-09
 */
@Repository
@CacheConfig(cacheNames = "ehcache")
public interface SpUserMapper extends  BaseSuperMapper<SpUser> {

    SpUser selectTest(Long id);

    //使用ehcache缓存技术
    @Select("select * from user where id=#{id}")
    @Cacheable
    public SpUser selectCache(@Param("id") int id);
}
