package com.huayu.languo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.TableFieldInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.function.Predicate;

/**
 * <p>
 * 测试自定义 mapper 父类
 * </p>
 */
public interface BaseSuperMapper<T> extends BaseMapper<T> {

    /**
     * 这里可以写自己的公共方法、注意不要让 mp 扫描到误以为是实体 Base 的操作
     *
     * @Date: 2019/5/27 15:15
     * @Param:
     * @Return:
     * @author: lxk
     */
    Integer deleteAll();

    /** 批量插入
     * @Date: 2019/5/27 15:15
     * @Param:
     * @Return:
     * @author: lxk
     */
    Integer insertBatchSomeColumn(List<T> list);

    Integer insertBatchSomeColumn(Predicate<TableFieldInfo> predicate, @Param("list") List<T> list);

}
