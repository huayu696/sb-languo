package com.huayu.languo.mapper;


import com.huayu.languo.model.po.ProductConfig;

public interface ProductConfigMapper extends BaseSuperMapper<ProductConfig> {
  /*  AutoSqlInjector ：
    BaseMapper提供了17个常用方法，但是有些需求这些方法还是不能很好的实现，那么怎么办呢？大家肯定会想到是在xml文件中写sql语句解决。
    这样确实可以，因为MP是只做增强不做改变，我们完全可以按照mybatis的原来的方式来解决。不过MP也提供了另一种解决办法，那就是自定义全局操作。
    所谓自定义全局操作，也就是我们可以在mapper中自定义一些方法，然后通过某些操作，让自定义的这个方法也能像BaseMapper的内置方法，
    供全局调用。接下来就看看如何实现(以deleteAll方法为例)。
    链接：https://www.jianshu.com/p/a4d5d310daf8
   */
}
