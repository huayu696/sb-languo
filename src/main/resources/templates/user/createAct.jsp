<!DOCTYPE HTML>
<html>
<head>
    <title>商家联合推广 - 京牛微店营销</title>
</head>
<body>
<div class="container-mid">
    <div class="container-content-frame">
        <div class="crm-main-content">
            <div class="open-promotion">
                <div class="pag-header">
                    <div class="page-header" style="margin: 10px 0 10px 10px;">
                        <strong style="font-size: 20px">联合营销 — 发起活动 </strong>
                    </div>
                </div>

                <div class="open-promotion-intro">
                    <div class="intro-welo">您将发起本次活动！</div>
                    <div class="intro-welo-left">
                        <ul class="intro-list">
                            <li>发起方拥有3大特权：</li>
                            <li>1、设定活动参与店铺数量，参与类目及推广数量</li>
                            <li>2、确定报名时间和推广时间</li>
                            <li>3、优先选择参加类目</li>
                        </ul>
                    </div>
                    <div class="intro-welo-right">
                        <ul class="intro-list">
                            <li>发起方的责任：</li>
                            <li>1、联系参与店铺，尽快达成一致</li>
                            <li>2、开发活动页面，包含参与店铺的商品</li>
                            <li>3、提交活动，让活动按期执行</li>
                        </ul>
                    </div>
                </div>

                <div>-------------</div>


                <div class="promotion-lists">
                    <div class="promotion-item">
                        <div class="info item">
                            <div class="sub-item">
                                <span class="text">活动店铺总数：</span>
                                <input type="text" class="form-control input-small input" v-model="contacts">
                                还需要1个店铺参与，每店最小推广数20000人
                            </div>
                            <div class="sub-item">
                                <span class="text">本店参加活动类目：</span>
                                最小推广客户数：
                                <input type="text" class="form-control input-small input" v-model="vocation">
                            </div>
                            <div class="sub-item">
                                <span class="text">参与店铺1  参加类目：</span>
                                最小推广客户数：
                                <input type="text" class="form-control input-small input" v-model="mobile"
                                       onkeyup="this.value=this.value.replace(/\D/g,'')" maxlength="11">
                            </div>
                            <div class="sub-item">
                                <span class="text">参与店铺1  参加类目：</span>
                                最小推广客户数：
                                <input type="text" class="form-control input-small input" v-model="qq"
                                       onkeyup="this.value=this.value.replace(/\D/g,'')">
                            </div>
                        </div>
                        <div class="hint">请填写真实的联系方式，以便商家之间顺利联系，确保参加活动不受影响</div>
                    </div>
                    <div>-------------</div>

                    <div class="promotion-item">
                        <div class="info item">
                            <div class="sub-item">
                                <span class="text">报名截止时间：</span>
                                <input type="text" class="form-control input-small input" v-model="contacts">
                            </div>
                            <div class="sub-item">
                                <span class="text">活动开始时间：</span>
                                <input type="text" class="form-control input-small input" v-model="vocation">
                            </div>
                            <div class="sub-item">
                                <span class="text">活动结束时间：</span>
                                <input type="text" class="form-control input-small input" v-model="mobile"
                                       onkeyup="this.value=this.value.replace(/\D/g,'')" maxlength="11">
                            </div>
                            <div class="sub-item">
                                <span class="text">活动名称：</span>
                                <input type="text" class="form-control input-small input" v-model="qq"
                                       onkeyup="this.value=this.value.replace(/\D/g,'')">
                            </div>
                            <div class="sub-item">
                                <span class="text">活动说明：</span>
                                <input type="text" class="form-control input-small input" v-model="qq"
                                       onkeyup="this.value=this.value.replace(/\D/g,'')">
                            </div>
                        </div>
                        <div class="hint">请填写真实的联系方式，以便商家之间顺利联系，确保参加活动不受影响</div>
                    </div>
                </div>


                <div class="btn-wrap">
                    <a href="javascript:;" class="btn btn-primary" v-if="isJoin==1" @click="saveServer">开通服务</a>
                    <a href="javascript:;" class="btn btn-primary" v-if="isJoin==4||isJoin==3||isJoin==2"
                       @click="saveServer">修改信息</a>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

</script>
</body>
</html>