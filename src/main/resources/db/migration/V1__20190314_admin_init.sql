

CREATE TABLE `sp_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nick_name` varchar(255) DEFAULT NULL,
  `sex` int(1) DEFAULT NULL,
  `register_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

INSERT INTO `sp_user` VALUES ('1', '89921218@qq.com', '1ee04e0b1cb5af7367c80c22e42efd8b', '土豆', '1', '2017-06-23 14:24:23');
-- INSERT INTO `sp_user` VALUES ('2', '2@qq.com', '1ee04e0b1cb5af7367c80c22e42efd8b', '土豆-2', '1', '2017-06-23 14:24:23');
INSERT INTO `sp_user` VALUES ('3', '3@qq.com', '1ee04e0b1cb5af7367c80c22e42efd8b', '土豆-3', '1', '2017-06-23 14:24:23');


CREATE TABLE `sp_user_info` (
  `nick` varchar(45) NOT NULL,
  `urgent_contacter` varchar(32) DEFAULT NULL COMMENT '紧急联系人',
  `urgent_mobile` varchar(59) DEFAULT NULL COMMENT '紧急联系人手机',
  `realname` varchar(32) DEFAULT NULL COMMENT '真实姓名',
  `card_id` varchar(20) DEFAULT NULL COMMENT '身份证ID',
  `card_pic` varchar(255) DEFAULT NULL COMMENT '身份证照片url',
  `company` varchar(100) DEFAULT NULL COMMENT '公司名称',
  `company_id` varchar(50) DEFAULT NULL COMMENT '公司证件ID',
  `company_pic` varchar(255) DEFAULT NULL COMMENT '公司证件照片url',
  `country` varchar(32) DEFAULT NULL COMMENT '国家',
  `need_notify` int(5) DEFAULT '0' COMMENT '是否需要启动金额阀值开关，1开启，null或其他为关闭，不需要',
  `notify_threshold` int(10) DEFAULT '0' COMMENT '剩余金额的预警阀值',
  `province` varchar(32) DEFAULT NULL COMMENT '省份',
  `city` varchar(32) DEFAULT NULL COMMENT '城市',
  `region` varchar(32) DEFAULT NULL COMMENT '地区',
  `address` varchar(100) DEFAULT NULL COMMENT '详细地址',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `business_scope` varchar(200) DEFAULT NULL COMMENT '公司经营范围，所属行业',
  `company_address` varchar(200) DEFAULT NULL COMMENT '公司地址',
  `corporation_name` varchar(100) DEFAULT NULL COMMENT '法人名称',
  `person_fail_msg` varchar(100) DEFAULT NULL COMMENT '个人验证失败原因',
  `company_fail_msg` varchar(100) DEFAULT NULL COMMENT '企业验证失败原因',
  `are_identify_company` int(2) DEFAULT '0' COMMENT '0或其他企业待认证，1企业认证审核失败2企业认证通过,3企业认证待审核',
  `are_identify_person` int(2) DEFAULT '0' COMMENT '0或其他个人待认证,1个人认证审核失败,2个人认证通过 3个人认证待审核',
  `identity_time` datetime DEFAULT NULL COMMENT '验证提交时间',
  `identiy_type` int(2) DEFAULT '1' COMMENT '验证类型，1个人验证，2企业验证',
  `test` int(2) DEFAULT NULL,
  PRIMARY KEY (`nick`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
