alter table sp_user add upd_time datetime DEFAULT NULL COMMENT '更新时间';
INSERT INTO `sp_user` VALUES ('2', '2222222@qq.com', '1ee04e0b1cb5af7367c80c22e42efd8b', '土豆-2', '1', '2017-06-23 14:24:23', '2017-06-23 14:24:23');




INSERT INTO `sp_user_info` VALUES ('15259295973', null, null, null, null, null, null, null, null, null, '0', null, null, null, null, null, '2018-01-18 14:14:04', '2018-01-18 14:14:04', null, null, null, null, null, '0', '0', null, '1', null);
INSERT INTO `sp_user_info` VALUES ('15606825675', null, null, null, null, null, null, null, null, null, '0', null, null, null, null, null, '2018-01-23 14:12:45', '2018-01-23 14:12:45', null, null, null, null, null, '0', '0', null, '1', null);


  CREATE TABLE `product_config` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `channel` int(2) DEFAULT NULL COMMENT '渠道编号',
  `prod_no` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '产品编号',
  `prd_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '产品名称',
  `st_time` datetime DEFAULT NULL COMMENT '产品开始有效时间',
  `ed_time` datetime DEFAULT NULL COMMENT '产品开始无效时间',
  `creater` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '产品创建人',
  `in_time` datetime DEFAULT NULL COMMENT '入库时间',
  `upd_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `channel` (`channel`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `product_rec_info` (
  `rec_no` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '推荐流水',
  `channel` int(2) DEFAULT NULL COMMENT '渠道编号',
  `tel` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT '电话号码',
  `uid` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '招行内部uid',
  `trans_no` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '渠道交易流水',
  `in_time` datetime DEFAULT NULL COMMENT '插入时间',
  `upd_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`rec_no`),
  KEY `channel_transNo` (`channel`,`trans_no`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
