var username = document.cookie.split(";")[0].split("=")[1];

//JS操作cookies方法!
//写cookies
//cookie的四个属性。这些属性用下面的格式加到字符串值后面： 
//name=<value>[; expires=<date>][; domain=<domain>][; path=<path>][; secure] 
//名称=<值>[; expires=<日期>][; domain=<域>][; path=<路径>][; 安全] 
//<value>, <date>, <domain> 和 <path> 应当用对应的值替换。<date> 应当使用GMT格式，可以使用Javascript脚本语言的日期类Date的.toGMTString() 方法得到这一GMT格式的日期值。方括号代表这项是可选的。
//比如在 [; secure]两边的方括号代表要想把cookie设置成安全的，就需要把"; secure" 加到cookie字符串值的后面。如果"; secure" 没有加到cookie字符串后面，那么这个cookie就是不安全的。
//不要把尖括号<> 和方括号[] 加到cookie里（除非它们是某些值的内容）。设置属性时，不限属性，可以用任何顺序设置。 


//JSP中对Cookie的操作: 类型 方法名 方法解释 
//String getComment() 返回cookie中注释,如果没有注释的话将返回空值. 
//String getDomain() 返回cookie中Cookie适用的域名. 使用getDomain() 方法可以指示浏览器把Cookie返回给同 一域内的其他服务器,而通常 Cookie只返回给与发送它的服务器名字完全相同的服务器。注意域名必须以点开始 
//int getMaxAge() 返回Cookie过期之前的最大时间，以秒计算。 
//String getName() 返回Cookie的名字 
//String getPath() 返回Cookie适用的路径。如果不指定路径，Cookie将返回给当前页面所在目录及其子目录下 的所有页面。 
//boolean getSecure() 如果浏览器通过安全协议发送cookies将返回true值，如果浏览器使用标准协议则返回false值。 
//String getValue() 返回Cookie的值。笔者也将在后面详细介绍getValue/setValue。 
//int getVersion() 返回Cookie所遵从的协议版本。 
//void setComment(String purpose) 设置cookie中注释 
//void setDomain(String pattern) 设置cookie中Cookie适用的域名 
//void setMaxAge(int expiry) 以秒计算，设置Cookie过期时间。 
//void setPath(String uri) 指定Cookie适用的路径。 
//void setSecure(boolean flag) 指出浏览器使用的安全协议，例如HTTPS或SSL。 
//void setValue(String newValue) cookie创建后设置一个新的值。 
//void setVersion(int v) 设置Cookie所遵从的协议版本 


function setCookie(name, value, time) {
    var strsec = getsec(time);
    var exp = new Date();
    exp.setTime(exp.getTime() + strsec * 1);
    document.cookie = name + "=" + escape(value) + ";expires=" + exp.toGMTString();
}

function getsec(str) {
    var str1 = str.substring(1, str.length) * 1;
    var str2 = str.substring(0, 1);
    if (str2 == "s") {
        return str1 * 1000;
    } else if (str2 == "h") {
        return str1 * 60 * 60 * 1000;
    } else if (str2 == "d") {
        return str1 * 24 * 60 * 60 * 1000;
    }
}

function getCookie(name) {
    var arr, reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");
    if (arr = document.cookie.match(reg))
        return unescape(arr[2]);
    else
        return null;
}


function delCookie(name) {
    var exp = new Date();
    exp.setTime(exp.getTime() - 1);
    var cval = getCookie(name);
    if (cval != null)
        document.cookie = name + "=" + cval + ";expires=" + exp.toGMTString();
}


//这是有设定过期时间的使用示例：
//s20是代表20秒
//h是指小时，如12小时则是：h12
//d是天数，30天则：d30
setCookie("name", "hayden", "s20");

//使用示例
setCookie("name", "hayden");
alert(getCookie("name"));

