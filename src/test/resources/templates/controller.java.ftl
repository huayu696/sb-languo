package ${package.Entity};

import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import ${cfg.basePackage}.model.po.${cfg.modelNameUpperCamel};
import ${cfg.basePackage}.handler.service.${cfg.modelNameUpperCamel}Service;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import com.huayu.languo.controller.AbstractSuperController;
import com.huayu.languo.model.dto.ResultDto;

/**
* @description ${cfg.tableName}（${cfg.tableComment}） 访问控制类
* @date     ${cfg.createTime}
* @author   ${cfg.author}
* @version  v1.0
*/
@RestController
@RequestMapping("/admin${cfg.baseRequestMapping}")
public class ${cfg.modelNameUpperCamel}Controller extends AbstractSuperController {

    @Autowired
    private ${cfg.modelNameUpperCamel}Service ${cfg.modelNameLowerCamel}Service;

    @PostMapping("/add")
    public ResultDto add(@RequestBody ${cfg.modelNameUpperCamel} ${cfg.modelNameLowerCamel}) {
        ${cfg.modelNameLowerCamel}Service.save(${cfg.modelNameLowerCamel});
        return ResultDto.success();
    }

    @DeleteMapping("/{id}")
    public ResultDto delete(@PathVariable Integer id) {
        ${cfg.modelNameLowerCamel}Service.removeById(id);
        return ResultDto.success();
    }

    @PutMapping
    public ResultDto update(@RequestBody ${cfg.modelNameUpperCamel} ${cfg.modelNameLowerCamel}) {
        ${cfg.modelNameLowerCamel}Service.update(new UpdateWrapper(${cfg.modelNameLowerCamel}));
        return ResultDto.success();
    }

    @GetMapping("/{id}")
    public ResultDto detail(@PathVariable Integer id) {
        ${cfg.modelNameUpperCamel} ${cfg.modelNameLowerCamel} = ${cfg.modelNameLowerCamel}Service.getById(id);
        return ResultDto.success(${cfg.modelNameLowerCamel}, Collections.emptyMap());
    }

    @GetMapping
    public ResultDto list(@RequestParam(defaultValue = "0") Integer curPage, @RequestParam(defaultValue = "12") Integer size,@RequestBody(required = false) ${cfg.modelNameUpperCamel} ${cfg.modelNameLowerCamel}) {
        IPage<${cfg.modelNameUpperCamel}> dataPage = ${cfg.modelNameLowerCamel}Service.page(new Page<${cfg.modelNameUpperCamel}>(curPage, size, Boolean.TRUE), new QueryWrapper<${cfg.modelNameUpperCamel}>().setEntity(${cfg.modelNameLowerCamel}));
        return ResultDto.success(dataPage, Collections.emptyMap());
    }
}
