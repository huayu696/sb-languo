package com.huayu.languo.mapper;

import com.huayu.languo.BaseTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.SQLException;

@Slf4j
public class TestDivSql extends BaseTest {


    private static final Logger LOG = LoggerFactory.getLogger(TestDivSql.class);

    @Autowired
    ProductConfigMapper productConfigMapper;

    /**
     * 测试 自定义 sql
     * @throws SQLException
     */
    @Test
    public void testInterceptor() throws SQLException {

        //List<User> list = new ArrayList<>();
        //list.add(new User("name22", TypeEnum.NORMAL, 22, new Date()));
        //list.add(new User("name33", TypeEnum.DISABLED, 33, new Date()));
        //list.add(new User("name44", TypeEnum.NORMAL, 44, new Date()));
        //Integer insertBatch = userMapper.insertBatchSomeColumn(list);
        ////FIXME  t -> !t.getProperty().equals("age") 带条件报错
        ////Integer insertBatch = userMapper.insertBatchSomeColumn(t -> !t.isLogicDelete(), list2);
        ////Integer insertBatch = userMapper.insertBatchSomeColumn(t -> !t.getProperty().equals("age"), list);
        //LOG.info("insertBatchSomeColumn 成功 1 ：" + insertBatch);

    }
}
