package com.huayu.languo.mapper;

import com.huayu.languo.model.po.SpUser;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;

/**
 * @author 刘雄康
 * @version v1.0
 * @description
 * @date 2019年05月23日
 */
public class SpUserMapperTest {

    private static SpUserMapper mapper;
    
    private static final Logger LOG = LoggerFactory.getLogger(SpUserMapperTest.class);


    @BeforeClass
    public static void setUpMybatisDatabase() {
        SqlSessionFactory builder = new SqlSessionFactoryBuilder()
                        .build(SpUserMapperTest.class.getClassLoader().getResourceAsStream("mybatisTestConfiguration/SpUserMapperTestConfiguration.xml"));
        //you can use builder.openSession(false) to not commit to database
        mapper = builder.getConfiguration().getMapper(SpUserMapper.class, builder.openSession(true));
    }

    @Test
    public void testSelectTest() throws FileNotFoundException {
        SpUser spUser = mapper.selectTest(1L);
        System.out.println(spUser);
    }


}
