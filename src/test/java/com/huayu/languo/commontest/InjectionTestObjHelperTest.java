package com.huayu.languo.commontest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.huayu.languo.InjectionTestObjHelper;
import com.huayu.languo.model.po.ProductConfig;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class InjectionTestObjHelperTest {

    private static final Logger LOG = LoggerFactory.getLogger(InjectionTestObjHelperTest.class);


    @Test
    public void testInjectionObjHelperGetEnumsConstraint() {

        InjectionTestObjHelper injectionTestObjHelper = new InjectionTestObjHelper();
        List<String> strings = Arrays.asList(
                "推荐人生成方式(1-随机生成，0-其他)" ,
                " (字典数据类型)01-业务考核，02-投放主体，03-付费方式" ,
                "是否需要启动金额阀值开关，1开启，null或其他为关闭，不需要" ,
                "是否是行内（0-行内，1-行外）" ,
                "0或其他企业待认证，1企业认证审核失败2企业认证通过,3企业认证待审核" ,
                "验证类型，1个人验证，2企业验证" ,
                "平台类型（1->京东 2->淘宝天猫）" ,
                "状态（0->上架 1->下架 -1->删除）" ,
                "状态（0a->上架 1b->下架 -1c->删除）" ,
                "状态（0a0->上架 1b1->下架 -1c-1->删除）" ,
                "状态（a0->上架 b1->下架 c-1->删除）" ,
                "用户类型 0表示普通用户 1表示高级用户 9表示vip" ,
                "是否需要启动金额阀值开关，1开启，null或其他为关闭，不需要" , "是否需要启动金额阀值开关，1开启，null或其他为关闭，不需要" ,
                "状态  0：禁用   1：正常" , "是否显示  0：否 1：是" , "0:不是 1:是" ,
                "0为需要短信验证，1为免审" ,
                "有效的子账号：子账号之间用json格式{1：子账号，2子账号}" ,
                "0或其他企业待认证，1企业认证审核失败2企业认证通过,3企业认证待审核" ,
                "1为数据在redis,还没发送。2为已开始发送。-1为发送完。-2为已手动删除" ,
                "1 为通道商相关信息 ；2 为短信组件使用方帐号等信息 ；3 和个数相关的配置 " ,
                "-1扣费失败，0等待提交,1提交中，2短信组件全提交成功，3提交失败，4等待审核中，5审核失败" ,
                "1发送,提交成功，0发送失败，2提交失败，-1余额不足,3审核中，待发送，4审核失败" ,
                "是否删除（ 0：否 1：是）" , "是否删除（0：否 ,1：是）" ,
                "是否删除( 0：否 1：是)" , "是否删除( 0否 1是)" ,
                "是否删除( 0否 ,1是)" , "是否删除( 0否 ,1是)" ,
                "是否删除( 0-否 ,1-是)" , "是否删除( 0-否 ,1-是)" ,
                "是否删除( 0-否 ;1-是)" ,
                "是否删除( 0-否 。1-是)" ,
                "是否删除( 0：否 。1：是)" ,
                "是否删除( 0:否 。1:是)" ,
                "是否删除( 0 否 。1 是)" ,
                " (字典数据类型)a01-业务考核，b02-投放主体，c03-付费方式" ,
                "----， (字典数据类型)a01-业务考核，b02-投d放e主f体，c03-付费方式333,222,----，cccc-付费方式，0000-付费方式 ,-1,-2,9.7,2.1---"
        );
        for (String string : strings) {
            System.out.println("准备解析--->");
            System.out.println(string);
            System.out.println(injectionTestObjHelper.getEnumsConstraint(string, 3, 2));
        }
    }

    @Test
    public void getFields() {
        List<Field> declaredFields1 = getDeclaredFields(ProductConfig.class);
        System.out.println(declaredFields1);
        for (Field field : declaredFields1) {
            System.out.println(field.getName());
        }
    }

    public <T> List<Field> getDeclaredFields(Class<T> aClass) {
        List<Field> objects = new ArrayList<>();
        objects.addAll(Arrays.asList(aClass.getDeclaredFields()));

        Class<? super T> superclass = aClass.getSuperclass();
        if (superclass instanceof Object && superclass.getSuperclass() == null && superclass.getName().equals("java.lang.Object")) {
            return objects;
        }
        objects.addAll(getDeclaredFields(superclass));
        return objects;
    }

    @Test
    public void testInjectionObjHelperTest() {
//        System.out.println( Math.pow(10, -2) >0);
        if (1 == 1) {
//            return;
        }

        InjectionTestObjHelper.InjectionConfig injectionConfig = new InjectionTestObjHelper.InjectionConfig();
        injectionConfig.setStrValPre("test_pre_");
        injectionConfig.setDataSourceUrl("jdbc:mysql://localhost:3306/lxk?useUnicode=true&characterEncoding=utf8&useSSL=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=Asia/Shanghai&tinyInt1isBit=false&autoReconnect=true&zeroDateTimeBehavior=convertToNull");
        injectionConfig.setDataSourceUserName("root");
        injectionConfig.setDataSourcePassWord("root");
        List<String> nullFields = new ArrayList<>();
        nullFields.add("spUsers.id");
//        nullFields.add("productRecInfo.id");
//        nullFields.add("id");
        injectionConfig.setNullFields(nullFields);
        InjectionTestObjHelper injectionTestObjHelper = new InjectionTestObjHelper(injectionConfig);
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
            ProductConfig injection = injectionTestObjHelper.injection(ProductConfig.class);
            LOG.info("generate data: \r \n{} " , objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(injection));
            // LOG.info("generate data: \r \n {}", JSONUtil.toJsonPrettyStr(injection));

            // QrCodePlatResultDto injection2 = injectionTestObjHelper.injection(new QrCodePlatResultDto());
            // LOG.info("generate data: \r \n {}", JSONUtil.toJsonPrettyStr(injection2));

            // Integer injection4 = injectionTestObjHelper.injection(Integer.valueOf(1111));
            // LOG.info("generate data: \r \n {}", injection4);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
    }
}
