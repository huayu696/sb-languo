package com.huayu.languo.commontest;

import lombok.Data;

import java.util.List;

public class InjectionTest {
    public static void main(String[] args) {
        int a = 1;
        Object aa = a;
        System.out.println(aa == null);
    }
}


@Data
class School {
    private Integer id;
    private String name;
    private List<Clazz> clazzs;
}


@Data
class Clazz {
    private Integer id;
    private String name;
    private List<Student> students;

}

@Data
class Student {
    private Integer stuId;
    private String name;
    private List<Clazz> clazzs;
}